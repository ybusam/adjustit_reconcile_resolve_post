﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ESRI.ArcGIS.Framework;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using System.Windows.Forms;

namespace DataReconcileAndPost
{
    public class DataReconcilePost : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        string retMsg = string.Empty;

        public IApplication pApp = ArcMap.Application;
        //public IMxDocument pMxDoc = ArcMap.Document;
        //public IMap pMap = (IMap)pMxDoc.ActiveView;
        public IMxDocument pMxDoc = ArcMap.Application.Document as IMxDocument;
        public IMap pMap = null;

        public frmDataReconcilePost pForm;

        public DataReconcilePost()
        {
        }
        protected override void OnClick()
        {
            ArcMap.Application.CurrentTool = null;
            pMap = pMxDoc.FocusMap;
            if (pMap.LayerCount == 0)
            {
                MessageBox.Show("Please add data to Arcmap");
                return;
            }

            if (pForm != null)
            {
                if (pForm.IsDisposed)
                {
                    pForm = new frmDataReconcilePost();
                    pForm.Show();
                }
                else
                {
                    pForm.BringToFront();
                }
            }
            else
            {
                pForm = new frmDataReconcilePost();
                pForm.Show();
            }
        }
    }
}
