﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using System.Configuration;
using System.Windows.Forms;



namespace DataReconcileAndPost
{
    class LogReports
    {
        private static StreamWriter objTextWriter = null;
        public static string strFilePath = string.Empty;
        private static string strEnd = string.Empty;
        public static bool blnerrors = false;

        /// <summary>
        /// Global variable decleration for application reports
        /// </summary>
        /// <param name="strFile"></param>

        private static IList<string> Strlst;

        public static string strLogPath = string.Empty;
        public static IList<string> lstSkipLayers
        {
            get { return Strlst; }
            set { Strlst = value; }
        }
        public static string strStartEnd
        {
            get { return strEnd; }
            set { strEnd = value; }
        }


        [STAThread]

        public static void createLogfile(string strFile, string logPath)
        {
            try
            {
                strLogPath = logPath;
                // check whether selected path exists or not
                if (Directory.Exists(strLogPath) == false)
                {
                    Directory.CreateDirectory(strLogPath);
                }
                //get current time to make filename                
                string strTimeStamp = DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second;
                //if the file already exists then append to the existing file
                strFilePath = strLogPath + "\\" + strTimeStamp + "_" + strFile;
                objTextWriter = new StreamWriter(strFilePath);
                objTextWriter.Close();
            }
            catch (Exception exce)
            {
                throw new Exception("Error Occured in createLogfile" + exce.Message);
            }
        }

        public static void writeLine(string strText)
        {
            if (objTextWriter != null)
            {
                objTextWriter = new StreamWriter(strFilePath, true);
                objTextWriter.WriteLine(strText);
                objTextWriter.Close();
            }
        }

        /// <summary>
        /// This method is called while closing of the application 
        /// </summary>
        public static void CloseLog()
        {
            try
            {
                if (objTextWriter != null)
                {
                    objTextWriter = new StreamWriter(strFilePath, true);
                    objTextWriter.WriteLine("Application Executed Successfully..!");
                    objTextWriter.Close();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public static void InitLogSummary(string LogName, string versionName, string logPath)
        {
            try
            {
                LogReports.createLogfile(LogName + ".log", logPath);
                LogReports.writeLine("Application Name :" + LogName);
                LogReports.writeLine("SDE Version Details :" + versionName);
                LogReports.writeLine("Executed By  :" + System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                LogReports.writeLine("******************************************************************************");
            }
            catch (Exception Ex)
            {
                LogReports.writeLine("Error @ initialize the log : " + Ex.Message);
            }
        }
    }
}
