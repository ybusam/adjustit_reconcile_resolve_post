﻿
namespace DataReconcileAndPost
{
    partial class frmDataReconcilePost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataReconcilePost));
            this.label1 = new System.Windows.Forms.Label();
            this.CmbVersions = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblParentVersion = new System.Windows.Forms.Label();
            this.btnExecute = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chose Child Version :";
            // 
            // CmbVersions
            // 
            this.CmbVersions.FormattingEnabled = true;
            this.CmbVersions.Location = new System.Drawing.Point(131, 16);
            this.CmbVersions.Name = "CmbVersions";
            this.CmbVersions.Size = new System.Drawing.Size(180, 21);
            this.CmbVersions.TabIndex = 1;
            this.CmbVersions.SelectedIndexChanged += new System.EventHandler(this.CmbVersions_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Parent Version :";
            // 
            // lblParentVersion
            // 
            this.lblParentVersion.AutoSize = true;
            this.lblParentVersion.Location = new System.Drawing.Point(131, 50);
            this.lblParentVersion.Name = "lblParentVersion";
            this.lblParentVersion.Size = new System.Drawing.Size(0, 13);
            this.lblParentVersion.TabIndex = 3;
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(236, 68);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(75, 23);
            this.btnExecute.TabIndex = 4;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.Location = new System.Drawing.Point(18, 73);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(212, 18);
            this.lblMsg.TabIndex = 5;
            // 
            // frmDataReconcilePost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 102);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnExecute);
            this.Controls.Add(this.lblParentVersion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CmbVersions);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDataReconcilePost";
            this.Text = "Data Reconcile & Post";
            this.Load += new System.EventHandler(this.frmDataReconcilePost_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbVersions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblParentVersion;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Label lblMsg;
    }
}