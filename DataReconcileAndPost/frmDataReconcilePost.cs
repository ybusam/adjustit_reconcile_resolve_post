﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using ESRI.ArcGIS.Framework;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geometry;
using System.Diagnostics.Tracing;

namespace DataReconcileAndPost
{
    public partial class frmDataReconcilePost : Form
    {
        public static IApplication pApp = ArcMap.Application;
        public static IMxDocument pMxDoc = ArcMap.Application.Document as IMxDocument;
        public static IMap pMap = null;

        public static Dictionary<string, string> dictVersions = null;
        public static ILayer pLyr = null;
        private IFeatureWorkspace featureWorkspace = null;
        private string strVersionDetails = string.Empty;
        public frmDataReconcilePost()
        {
            InitializeComponent();
        }

        private void frmDataReconcilePost_Load(object sender, EventArgs e)
        {
            try
            {
                //load the versions to drop-down list
                pMap = pMxDoc.FocusMap;
                dictVersions = GetLayerFromMap(pMap);
                if (dictVersions.Count == 0)
                {
                    MessageBox.Show("Invalid versions to post.", "Validate Versions"); return;
                }
                foreach (var item in dictVersions)
                {
                    CmbVersions.Items.Add(item.Key);
                }
                strVersionDetails = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static bool GetWorkspaceVersions(ILayer pLyr, ref Dictionary<string, string> dictVersions)
        {
            IWorkspace pWS = null; bool blnStatus = false;
            try
            {
                pWS = ((IDataset)pLyr).Workspace;
                IVersionedWorkspace versWS = (IVersionedWorkspace)pWS;

                IEnumVersionInfo versInfo = versWS.Versions;
                IVersionInfo verInfo = (IVersionInfo)versInfo.Next();

                while (verInfo != null)
                {
                    if (verInfo.VersionName != "sde.DEFAULT" && !dictVersions.ContainsKey(verInfo.VersionName))
                    {
                        dictVersions.Add(verInfo.VersionName, (verInfo.Parent).VersionName); blnStatus = true;
                    }
                    verInfo = (IVersionInfo)versInfo.Next();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("EXCP @ GetWorkspaceVersions " + ex.Message);
                LogReports.writeLine(ex.Message); blnStatus = false;
            }
            finally
            {
                if (pWS != null) { Marshal.ReleaseComObject(pWS); pWS = null; }
            }
            return blnStatus;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            try
            {
                //get the workspace for child version selected
                string versionName = CmbVersions.SelectedItem.ToString();

                //create log file
                string path = @"C:\DataReconcilePost";
                LogReports.InitLogSummary("DataReconcilePost", strVersionDetails, path);

                LogReports.writeLine("Valid Versions: " + dictVersions.Count);

                IWorkspace pWS = ((IDataset)pLyr).Workspace;
                IVersionedWorkspace versWS = (IVersionedWorkspace)pWS;
                IVersion selectedVersion = versWS.FindVersion(versionName);

                featureWorkspace = (IFeatureWorkspace)selectedVersion;
                ReconcileAndPostEditsRetry((IWorkspace)selectedVersion, true);

                MessageBox.Show("Post Completed. Check latest log file in :" + path, "Post Status");

                LogReports.CloseLog();
                //this.Close();
            }
            catch (Exception ex)
            {
                LogReports.writeLine(ex.Message);
            }
        }

        public void OnConflictsDetected(ref bool conflictsRemoved, ref bool errorOccurred, ref string errorString)
        {
            try
            {
                IVersionEdit4 versionEdit4 = (IVersionEdit4)featureWorkspace;

                // Get the various versions on which to output information.
                IFeatureWorkspace commonAncestorFWorkspace = (IFeatureWorkspace)versionEdit4.CommonAncestorVersion;
                IFeatureWorkspace preReconcileFWorkspace = (IFeatureWorkspace)versionEdit4.PreReconcileVersion;
                IFeatureWorkspace reconcileFWorkspace = (IFeatureWorkspace)versionEdit4.ReconcileVersion;

                IEnumConflictClass enumConflictClass = versionEdit4.ConflictClasses;
                IConflictClass conflictClass = null;
                while ((conflictClass = enumConflictClass.Next()) != null)
                {
                    IDataset dataset = (IDataset)conflictClass;

                    // Make sure the class is a feature class.
                    if (dataset.Type == esriDatasetType.esriDTFeatureClass)
                    {
                        String datasetName = dataset.Name;
                        IFeatureClass featureClass = featureWorkspace.OpenFeatureClass(datasetName);

                        LogReports.writeLine("Conflicts on feature class :" + datasetName);

                        // Get all UpdateUpdate conflicts.
                        ISelectionSet updateUpdates = conflictClass.UpdateUpdates;
                        if (updateUpdates.Count > 0)
                        {
                            // Get conflict feature classes on the three reconcile versions.
                            IFeatureClass featureClassPreReconcile = preReconcileFWorkspace.OpenFeatureClass(datasetName);
                            IFeatureClass featureClassReconcile = reconcileFWorkspace.OpenFeatureClass(datasetName);
                            IFeatureClass featureClassCommonAncestor = commonAncestorFWorkspace.OpenFeatureClass(datasetName);

                            // Iterate through each OID, outputting information.
                            IEnumIDs enumIDs = updateUpdates.IDs;
                            int oid = -1;
                            while ((oid = enumIDs.Next()) != -1)
                            //Loop through all conflicting features. 
                            {
                                LogReports.writeLine("Update conflicts on Class : " + datasetName + " OID : " + oid);

                                // Get conflict feature on the three reconcile versions.
                                IFeature featurePreReconcile = featureClassPreReconcile.GetFeature(oid);
                                IFeature featureReconcile = featureClassReconcile.GetFeature(oid);
                                IFeature featureCommonAncestor = featureClassCommonAncestor.GetFeature(oid);

                                // Check to make sure each shape is different than the common ancestor (conflict is on shape field).
                                if (IsShapeInConflict(featureCommonAncestor, featurePreReconcile, featureReconcile))
                                {
                                    LogReports.writeLine(" Shape attribute has changed on both versions. Class: " + datasetName + " OID : " + oid);

                                    // Geometries are in conflict ... merge geometries.
                                    try
                                    {
                                        IConstructMerge constructMerge = new
                                            GeometryEnvironmentClass();
                                        IGeometry newGeometry = constructMerge.MergeGeometries
                                            (featureCommonAncestor.ShapeCopy,
                                            featureReconcile.ShapeCopy,
                                            featurePreReconcile.ShapeCopy);

                                        // Setting new geometry as a merge between the two versions.
                                        IFeature feature = featureClass.GetFeature(oid);
                                        feature.Shape = newGeometry;
                                        feature.Store();
                                        updateUpdates.RemoveList(1, ref oid);
                                        conflictsRemoved = true;
                                    }
                                    catch (COMException comExc)
                                    {
                                        // Check if the error is from overlapping edits.
                                        if (comExc.ErrorCode == (int)
                                            fdoError.FDO_E_WORKSPACE_EXTENSION_DATASET_CREATE_FAILED || comExc.ErrorCode == (int)fdoError.FDO_E_WORKSPACE_EXTENSION_DATASET_DELETE_FAILED)
                                        {
                                            // Edited areas overlap.
                                            LogReports.writeLine(" Error from overlapping edits on feature :" + oid);
                                            LogReports.writeLine(" Error Message: " + comExc.Message);
                                            LogReports.writeLine(" Can't merge overlapping edits to same feature.");
                                        }
                                        else
                                        {
                                            // Unexpected COM exception, throw this to the exception handler.
                                            LogReports.writeLine(comExc.Message);
                                        }
                                    }
                                }
                                else
                                {
                                    LogReports.writeLine(" Shape field not in conflict: merge not necessary ... ");
                                }
                            }
                        }
                    }
                }
            }
            catch (COMException comExc)
            {
                LogReports.writeLine("Error Message: " + comExc.Message + " Error Code:" + comExc.ErrorCode);
            }
            catch (Exception exc)
            {
                LogReports.writeLine(exc.Message);
            }
        }

        // Method to determine if the shape field is in conflict.
        private bool IsShapeInConflict(IFeature commonAncestorFeature, IFeature preReconcileFeature, IFeature reconcileFeature)
        {
            // 1st check: Common Ancestor with PreReconcile.
            // 2nd check: Common Ancestor with Reconcile. 
            // 3rd check: Reconcile with PreReconcile (case of same change on both versions).
            if (IsGeometryEqual(commonAncestorFeature.ShapeCopy,
                preReconcileFeature.ShapeCopy) || IsGeometryEqual
                (commonAncestorFeature.ShapeCopy, reconcileFeature.ShapeCopy) ||
                IsGeometryEqual(reconcileFeature.ShapeCopy,
                preReconcileFeature.ShapeCopy))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // Method returning if two shapes are equal to one another.
        private bool IsGeometryEqual(IGeometry shape1, IGeometry shape2)
        {
            if (shape1 == null & shape2 == null)
            {
                return true;
            }
            else if (shape1 == null ^ shape2 == null)
            {
                return false;
            }
            else
            {
                IClone clone1 = (IClone)shape1;
                IClone clone2 = (IClone)shape2;
                return clone1.IsEqual(clone2);
            }
        }

        private void CmbVersions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string versionName = CmbVersions.SelectedItem.ToString();

                var keyValuePair = dictVersions.Single(x => x.Key == versionName);
                lblParentVersion.Text = keyValuePair.Value;

                strVersionDetails = versionName + "|" + keyValuePair.Value;
            }
            catch (Exception ex)
            {
                MessageBox.Show("@CmbVersions_SelectedIndexChanged " + ex.Message);
            }
        }

        public static Dictionary<string, string> GetLayerFromMap(IMap map)
        {
            pLyr = null;
            Dictionary<string, string> dictVersions = new Dictionary<string, string>();
            for (int i = 0; i < map.LayerCount; i++)
            {
                pLyr = map.Layer[i];
                if (pLyr is IFeatureLayer)
                {
                    // iterate thru the layers and get collection of versions to a dictionary
                    if (true == GetWorkspaceVersions(pLyr, ref dictVersions)) break;
                }
                pLyr = null;
            }
            return dictVersions;
        }

        internal bool ReconcileAndPostEditsRetry(IWorkspace workspace, bool canPost)
        {
            int retry = 0;
            bool posted = false;
            try
            {
                while (!posted)
                {
                    try
                    {
                        ReconcileAndPostEdits(workspace, canPost);
                        posted = true;
                    }
                    catch (Exception ex)
                    {
                        if (++retry <= 3)
                        {
                            ReconcileAndPostEdits(workspace, canPost);
                            posted = true;
                            LogReports.writeLine("Data posted");
                        }
                        else
                        {
                            throw new Exception(string.Format("Failure posting edits after 3 retries. {0}", ex.Message));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("@ReconcileAndPostEditsRetry ", ex.Message);
                LogReports.writeLine("Error Message: " + ex.Message);
            }
            return posted;
        }

        private void ReconcileAndPostEdits(IWorkspace workspace, bool canPost)
        {
            IVersion2 version = (IVersion2)workspace;
            IVersionEdit4 versionEdit = (IVersionEdit4)workspace;
            IWorkspaceEdit workspacedEdit = (IWorkspaceEdit)workspace;
            try
            {
                if (version.HasParent() != false)
                {
                    workspacedEdit.StartEditing(false);
                    workspacedEdit.StartEditOperation();

                    bool acquireLock = true;  // locking reconciles are safer than non-locking T F T T
                    bool abortIfConflicts = false;//F to identify conflicts
                    bool childWins = true;//org T
                    bool columnLevel = true; //org T

                    Boolean conflictsDetected = versionEdit.Reconcile4("SDE.DEFAULT", acquireLock, abortIfConflicts, childWins, columnLevel);

                    if (conflictsDetected == true)
                    {
                        LogReports.writeLine("Conflicts identified.");
                        bool blnSt1 = false, blnSt2 = false; string strst = string.Empty;
                        OnConflictsDetected(ref blnSt1, ref blnSt2, ref strst);
                    }
                    //else if (versionEdit.CanPost() && canPost)
                    //{
                    versionEdit.Post("SDE.DEFAULT");
                    //}

                    workspacedEdit.StopEditOperation();
                    workspacedEdit.StopEditing(true);
                }
            }
            catch (COMException ex)
            {
                MessageBox.Show("@ReconcileAndPostEdits ", ex.Message);
                LogReports.writeLine("Error Message: " + ex.Message + " Error Code:" + ex.ErrorCode);
            }
        }


    }
}
